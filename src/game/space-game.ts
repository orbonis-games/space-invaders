import { Tween } from "@tweenjs/tween.js";
import { Game } from "libs/core-game/src/game/game";
import { GameOver } from "libs/core-game/src/game/renderables/game-over";
import { Music } from "libs/core-game/src/game/renderables/music";
import { ScoreBoard } from "libs/core-game/src/game/renderables/score-board";
import { IsDebug } from "libs/core-game/src/utils/arguments";
import { DisplayObject } from "pixi.js";
import { Alien } from "./alien";
import { AlienSwarm } from "./alien-swarm";
import { Instructions } from "./instructions";
import { Mothership } from "./mothership";
import { Ship } from "./ship";
import { StarField } from "./star-field";

export class SpaceGame extends Game {
    private starField?: StarField;
    private ship?: Ship;
    private alienSwarm?: AlienSwarm;
    private mothership?: Mothership;
    private gameOver?: GameOver;
    private scoreBoard?: ScoreBoard;
    private instructions?: Instructions;
    private music?: Music;

    public async init(canvas: HTMLCanvasElement): Promise<void> {
        super.init(canvas, [
            { id: "shoot", url: "./assets/shoot.wav" },
            { id: "hit", url: "./assets/hit.wav" },
            { id: "explosion", url: "./assets/explosion.wav" }
        ]);
    }

    public prerender(delta: number): void {
        super.prerender(delta);

        const projectile: DisplayObject | undefined = this.ship?.getProjectile();
        this.alienSwarm?.update(projectile);
        if (this.mothership?.checkCollision(projectile)) {
            this.ship?.destroyProjectile();
            this.scoreBoard?.increaseScore(this.mothership!.getScore());
        }

        const alienProjectiles: DisplayObject[] | undefined = this.alienSwarm?.getAlienProjectiles();
        if (alienProjectiles) {
            for (const projectile of alienProjectiles) {
                if (this.ship?.checkCollision(projectile)) {
                    this.alienSwarm?.destroyProjectile(projectile);
                    if (!this.ship?.isAlive()) {
                        this.completeGame(false);
                    }
                }
            }
        }

    }

    protected async initRenderables(): Promise<void> {
        if (this.app) {
            this.starField = new StarField("star-field", this.app);
            this.ship = new Ship("ship", this.app);
            this.alienSwarm = new AlienSwarm("swarm", this.app);
            this.mothership = new Mothership("mothership", this.app);
            this.gameOver = new GameOver("game-over", this.app, { family: "VT323", color: 0xFFFFFF });
            this.scoreBoard = new ScoreBoard("score-board", this.app, { family: "VT323", color: 0xFFFFFF });
            this.instructions = new Instructions("instruction", this.app, { family: "VT323", color: 0xFFFFFF });
            this.music = new Music("music", this.app);

            await super.initRenderables(
                this.starField,
                this.ship,
                this.alienSwarm,
                this.mothership,
                this.scoreBoard,
                this.gameOver,
                this.instructions,
                this.music
            );

            this.gameOver.show("Space Invaders", "start", () => {
                this.scoreBoard?.show();
                this.setupGame();
            });

            this.alienSwarm.onAlienDestroyed = (alien: Alien) => {
                this.ship?.destroyProjectile();
                this.scoreBoard?.increaseScore(alien.getScore());
            };

            this.alienSwarm.onSwarmDefeated = () => this.completeGame(true);
            this.alienSwarm.onSwarmInvasion = () => this.completeGame(false);

            if (IsDebug()) {
                window.addEventListener("keyup", (e) => {
                    if (e.key === "End") {
                        this.shakeScreen();
                    }
                });
            }
        }
    }

    private setupGame(): void {
        this.instructions?.setVisible(false);
        this.music?.playMusic();
        this.alienSwarm?.reset();
        this.alienSwarm?.start();
        this.ship?.reset();
        this.ship?.setActive(true);
        this.mothership?.reset();
        this.mothership?.setActive(true);
    }

    private completeGame(win: boolean): void {
        this.ship?.setActive(false);
        this.alienSwarm?.setActive(false);
        this.mothership?.setActive(false);
        this.ship?.destroyProjectile();
        if (!win) {
            this.ship?.destroy();
            this.shakeScreen();
            this.alienSwarm?.resetLayout();
        }
        window.setTimeout(() => {
            this.scoreBoard?.hide();
            this.alienSwarm?.hideLevel();
            this.instructions?.setVisible(true);
            this.gameOver?.show(
                (win) ? "Victory!" : "Game Over",
                (win) ? "play again" : "try again",
                () => {
                    this.setupGame();
                    if (win) {
                        this.scoreBoard?.show();
                    } else {
                        this.scoreBoard?.resetScore();
                    }
                },
                this.scoreBoard?.getScore()
            );
        }, 500);
    }

    private shakeScreen(): Promise<void> {
        return new Promise((resolve) => {
            if (this.app) {
                new Tween(this.app.stage)
                    .to({ x: 10 }, 100)
                    .yoyo(true)
                    .repeat(9)
                    .onComplete(() => resolve())
                    .start();
            } else {
                resolve();
            }
        });
    }
}