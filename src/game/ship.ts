import { CollidableShape } from "libs/core-game/src/game/collision";
import { Renderable } from "libs/core-game/src/game/renderable";
import { sfx } from "libs/core-game/src/game/sfx";
import { Emitter, EmitterConfig } from "pixi-particles";
import { Container, DisplayObject, Graphics, Rectangle, Texture } from "pixi.js";

export class Ship extends Renderable {
    public static readonly MAX_LIVES: number = 3;

    private readonly speed: number = 200;
    private readonly fireSpeed: number = 600;

    private container?: Container;
    private graphic?: DisplayObject;
    private collision?: Rectangle;

    private liveDisplay?: Graphics;

    private explosionContainer?: Container;
    private explosion?: Emitter;

    private leftTrailContainer?: Container;
    private leftTrail?: Emitter;

    private rightTrailContainer?: Container;
    private rightTrail?: Emitter;

    private moveDelta: number = 0;
    private active: boolean = false;

    private projectile?: Graphics;
    private firing: boolean = false;

    private lives: number = Ship.MAX_LIVES;

    public init(): Promise<void> {
        return new Promise((resolve) => {
            this.app.loader.reset();
            this.app.loader.add("particle", "./assets/particle.png");
            this.app.loader.add("explosion", "./assets/explosion.json");
            this.app.loader.add("trail", "./assets/trail.json");
            this.app.loader.load(() => {
                this.container = new Container();
                this.graphic = this.createShip();
                this.liveDisplay = this.redrawLiveDisplay();
                this.container.addChild(this.graphic);
                this.container.addChild(this.liveDisplay);
                this.app.stage.addChild(this.container);

                const texture: Texture = this.app.loader.resources["particle"].texture;
                const configExplosion: EmitterConfig = this.app.loader.resources["explosion"].data;
                const configTrail: EmitterConfig = this.app.loader.resources["trail"].data;

                this.explosionContainer = new Container();
                this.container.addChild(this.explosionContainer);
                this.explosionContainer.y += 20;
                this.explosionContainer.scale.set(2, 2);

                this.explosion = new Emitter(this.explosionContainer, texture, configExplosion);

                this.leftTrailContainer = new Container();
                this.container.addChild(this.leftTrailContainer);

                this.leftTrail = new Emitter(this.leftTrailContainer, texture, configTrail);
                this.leftTrailContainer.angle = 180;
                this.leftTrailContainer.x = -38;
                this.leftTrailContainer.y = 40;

                this.rightTrailContainer = new Container();
                this.container.addChild(this.rightTrailContainer);
                this.rightTrailContainer.x = 38;
                this.rightTrailContainer.y = 40;

                this.collision = new Rectangle(this.container.x, this.container.y, this.container.width, this.container.height - 20);

                this.rightTrail = new Emitter(this.rightTrailContainer, texture, configTrail);
    
                this.reset();
                this.setupControls();
    
                resolve();
            });
        });
    }

    public reset(): void {
        if (this.container && this.collision && this.graphic && this.liveDisplay) {
            this.container.x = this.app.view.width / 2;
            this.container.y = 900;
            this.collision.x = (this.app.view.width / 2) - (this.container.width / 2);
            this.collision.y = 920;
            this.moveDelta = 0;
            this.firing = false;
            this.graphic.visible = true;
            this.liveDisplay.visible = true;
            this.lives = Ship.MAX_LIVES;
            this.redrawLiveDisplay();
        }
    }

    public setActive(value: boolean): void {
        this.active = value;
    }

    public checkCollision(projectile?: DisplayObject): boolean {
        if (this.collision && projectile && projectile.transform && this.active) {
            const verticalCheck: boolean =
                (projectile.x >= this.collision.x - (this.collision.width / 2)) &&
                (projectile.x <= this.collision.x + (this.collision.width / 2));
            const horizontalCheck: boolean =
                (projectile.y >= this.collision.y - (this.collision.height / 2)) &&
                (projectile.y <= this.collision.y + (this.collision.height / 2));
            
            const collision: boolean = verticalCheck && horizontalCheck;
            if (collision) {
                this.damage();
            }
            return collision;
        } else {
            return false;
        }
    }

    public isAlive(): boolean {
        return this.lives > 0;
    }

    public render(delta: number): void {
        if (this.active) {
            if (this.container && this.collision) {
                this.container.x += this.moveDelta * this.speed * delta;
                this.container.x = Math.max(200, Math.min(800, this.container.x));

                this.collision.x = this.container.x;
            }

            if (this.firing) {
                this.fireGun();
            }
    
            if (this.projectile) {
                this.projectile.y -= this.fireSpeed * delta;
    
                if (this.projectile.y <= 180) {
                    this.projectile.destroy();
                    this.projectile = undefined;
                }
            }
        }

        if (this.leftTrail && this.rightTrail) {
            this.leftTrail.emit = this.moveDelta > 0 && this.active;
            this.rightTrail.emit = this.moveDelta < 0 && this.active;
        }

        this.explosion?.update(delta);
        this.leftTrail?.update(delta);
        this.rightTrail?.update(delta);
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    public getProjectile(): DisplayObject | undefined {
        return this.projectile;
    }

    public destroyProjectile(): void {
        if (this.projectile) {
            this.projectile.destroy();
            this.projectile = undefined;
        }
    }

    public destroy(): void {
        if (this.graphic && this.liveDisplay) {
            this.explosion?.playOnce();
            this.graphic.visible = false;
            this.liveDisplay.visible = false;

            sfx.play("explosion");
        }
    }

    public damage(): void {
        this.lives--;
        this.redrawLiveDisplay();
        if (this.lives > 0) {
            this.explosion?.playOnce();

            sfx.play("hit");
        } else {
            this.destroy();
        }
    }

    private createShip(): DisplayObject {
        const graphic: Graphics = new Graphics();

        graphic.lineStyle(2, 0xFFFFFF);
        graphic.beginFill(0x000000);
        graphic.drawRect(-2, 0, 4, 10);
        graphic.drawPolygon([
            2, 8,
            10, 15,
            15, 15,
            15, 20,
            -15, 20,
            -15, 15,
            -10, 15,
            -2, 8
        ]);
        graphic.drawPolygon([
            15, 20,
            25, 20,
            30, 35,
            -30, 35,
            -25, 20,
            -15, 20
        ]);
        graphic.drawPolygon([
            30, 35,
            35, 35,
            35, 45,
            30, 45,
            15, 35
        ]);
        graphic.drawPolygon([
            -30, 35,
            -35, 35,
            -35, 45,
            -30, 45,
            -15, 35
        ]);
        graphic.endFill();

        graphic.cacheAsBitmap = true;

        return graphic;
    }

    private redrawLiveDisplay(): Graphics {
        if (!this.liveDisplay) {
            this.liveDisplay = new Graphics();
            this.liveDisplay.y = 27;
        }

        this.liveDisplay.clear();
        for (let i = 0; i < Ship.MAX_LIVES; i++) {
            if (this.lives > i) {
                this.liveDisplay.beginFill(0xFFFFFF);
                this.liveDisplay.drawCircle(-10 + (i * 10), 0, 4);
                this.liveDisplay.endFill();
            } else {
                this.liveDisplay.lineStyle(2, 0xFFFFFF);
                this.liveDisplay.drawCircle(-10 + (i * 10), 0, 4);
            }
        }

        return this.liveDisplay;
    }

    private fireGun(): void {
        if (!this.projectile && this.container && this.active) {
            this.projectile = new Graphics();

            this.projectile.lineStyle(2, 0xFFFFFF);
            this.projectile.drawRect(-2, 0, 4, 5);
            this.projectile.x = this.container.x;
            this.projectile.y = this.container.y - 20;

            this.app.stage.addChild(this.projectile);

            sfx.play("shoot");
        }
    }

    private setupControls(): void {
        window.addEventListener("keydown", (e) => {
            switch (e.key) {
                case "ArrowLeft":
                    this.moveDelta = -1;
                    break;
                case "ArrowRight":
                    this.moveDelta = 1;
                    break;
                case "ArrowUp":
                case " ":
                    this.firing = true;
                    break;
            }
        });

        window.addEventListener("keyup", (e) => {
            switch (e.key) {
                case "ArrowLeft":
                    if (this.moveDelta < 0) {
                        this.moveDelta = 0;
                    }
                    break;
                case "ArrowRight":
                    if (this.moveDelta > 0) {
                        this.moveDelta = 0;
                    }
                    break;
                case "ArrowUp":
                case " ":
                    this.firing = false;
                    break;
            }
        });
    }
}