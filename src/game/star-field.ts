import { CollidableShape } from "libs/core-game/src/game/collision";
import { Renderable } from "libs/core-game/src/game/renderable";
import { Emitter, EmitterConfig } from "pixi-particles";
import { Container, Texture } from "pixi.js";

export class StarField extends Renderable {
    private container?: Container;
    private stars?: Emitter;

    public init(): Promise<void> {
        return new Promise((resolve) => {
            this.app.loader.reset();
            this.app.loader.add("particle", "./assets/particle.png");
            this.app.loader.add("stars", "./assets/stars.json");
            this.app.loader.load(() => {
                this.container = new Container();
                this.app.stage.addChild(this.container);

                const texture: Texture = this.app.loader.resources["particle"].texture;
                const config: EmitterConfig = this.app.loader.resources["stars"].data;

                this.stars = new Emitter(this.container, texture, config);
                this.container.x = this.app.view.width / 2;
                this.container.y = this.app.view.height / 2;
                this.stars.update(10);

                resolve();
            });
        });
    }

    public render(delta: number): void {
        this.stars?.update(delta);
    }

    public getColliders(): CollidableShape[] {
        return [];
    }
}