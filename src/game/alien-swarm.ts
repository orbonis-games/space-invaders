import { CollidableShape } from "libs/core-game/src/game/collision";
import { Renderable } from "libs/core-game/src/game/renderable";
import { IsDebug } from "libs/core-game/src/utils/arguments";
import { Application, Container, DisplayObject, Point, Text } from "pixi.js";
import { Alien } from "./alien";
import { Swarms } from "./swarms";

export enum SwarmMoveDirection {
    Up, Down, Left, Right
}

export interface SwarmLayout {
    grid: number[][];
}

export class AlienSwarm extends Renderable {
    public static readonly SWARM_MOVE_GRID: Point = new Point(6, 10);
    public static readonly SWARM_INVASION_MARKER: number = 900;
    public static readonly MIN_FIRE_TIME_SECONDS: number = 1;
    public static readonly MAX_FIRE_TIME_SECONDS: number = 3;

    public onAlienDestroyed?: (alien: Alien) => void;
    public onSwarmDefeated?: () => void;
    public onSwarmInvasion?: () => void;

    private container?: Container;
    private levelLabel?: Text;
    private aliens: Array<Alien | undefined>[] = [];
    private active: boolean = false;
    private swarmDirection: SwarmMoveDirection = SwarmMoveDirection.Right;
    private swarmPosition: Point = new Point(0, 0);

    private fireTimer: number = 0;

    private projectiles: DisplayObject[] = [];

    private layoutIndex: number = 0;
    private level: number = 0;

    constructor(id: string, app: Application) {
        super(id, app);

        if (IsDebug()) {
            window.addEventListener("keyup", (e) => {
                if (e.key === "End") {
                    for (const alien of this.getAlienArray()) {
                        window.setTimeout(() => alien.destroy(), 1000 * Math.random())
                    }
                }
            });
        }
    }

    public async init(): Promise<void> {
        if (!this.container) {
            this.container = new Container();
            this.app.stage.addChild(this.container);

            this.levelLabel = new Text("");
            this.levelLabel.style.fontFamily = "VT323";
            this.levelLabel.style.fontSize = 30;
            this.levelLabel.style.fill = 0xFFFFFF;
            this.levelLabel.anchor.set(0.5, 1);
            this.levelLabel.x = this.app.view.width / 2;
            this.levelLabel.y = 170;

            this.container.addChild(this.levelLabel);
        }

        for (const alien of this.getAlienArray()) {
            alien.destroy(true);
        }

        this.aliens = [];
        for (let x = 0; x < Swarms[this.layoutIndex].grid.length; x++) {
            const aliens: Array<Alien | undefined> = [];
            for (let y = 0; y < Swarms[this.layoutIndex].grid[x].length; y++) {
                if (Swarms[this.layoutIndex].grid[x][y] > 0) {
                    aliens.push(new Alien(`alien[${x},${y}]`, this.app, this.container, new Point(x, y), Swarms[this.layoutIndex].grid[x][y] === 2));
                } else {
                    aliens.push(undefined);
                }
            }
            this.aliens.push(aliens);
        }

        for (const alien of this.getAlienArray()) {
            await alien.init();
        }
    }

    public reset(): void {
        this.swarmDirection = SwarmMoveDirection.Right;
        this.swarmPosition = new Point(Math.round(AlienSwarm.SWARM_MOVE_GRID.x / 2), 0);
        this.getAlienArray().forEach((x) => x.reset());
        this.resetFireTimer();
        if (this.levelLabel) {
            this.levelLabel.text = "";
        }
    }

    public hideLevel(): void {
        if (this.levelLabel) {
            this.levelLabel.text = "";
        }
    }

    public render(delta: number): void {
        this.getAlienArray().forEach((x) => x.render(delta));

        if (this.active) {
            this.fireTimer -= delta;
    
            if (this.fireTimer <= 0) {
                this.fireGun();
                this.resetFireTimer();
            }
        }
    }

    public getAlienProjectiles(): DisplayObject[] {
        this.projectiles = this.projectiles.filter((x) => x !== undefined && x.transform !== null);
        return this.projectiles;
    }

    public destroyProjectile(projectile: DisplayObject): void {
        this.projectiles.splice(this.projectiles.indexOf(projectile), 1);
        projectile.destroy();
    }

    public update(projectile?: DisplayObject): void {
        if (this.active) {
            const aliens: Alien[] = this.getAlienArray();
            if (projectile) {
                for (const alien of aliens) {
                    if (alien.checkCollision(projectile)) {
                        if (this.onAlienDestroyed) {
                            this.onAlienDestroyed(alien);
                        }
                    }
                }
            }

            if (aliens.filter((x) => x.isAlive()).length === 0) {
                if (this.onSwarmDefeated) {
                    this.onSwarmDefeated();
                }
            }

            for (const alien of aliens) {
                if (alien.hasInvaded()) {
                    if (this.onSwarmInvasion) {
                        this.onSwarmInvasion();
                    }
                    break;
                }
            }
        }
    }

    public setActive(value: boolean): void {
        this.active = value;
        this.getAlienArray().forEach((x) => {
            x.setActive(value);
        });
    }

    public start(): void {
        this.init().then(() => {
            this.layoutIndex++;
            this.level++;
            if (this.levelLabel) {
                this.levelLabel.text = `Level: ${this.level}`;
            }
            if (this.layoutIndex === Swarms.length) {
                this.layoutIndex = 0;
            }
            this.setActive(true);
            window.setTimeout(() => {
                this.moveSwarm();
            }, 250);
        });
    }

    public resetLayout(): void {
        this.layoutIndex = 0;
        this.level = 0;
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    private async moveSwarm(): Promise<void> {
        if (this.swarmPosition.x === AlienSwarm.SWARM_MOVE_GRID.x) {
            if (this.swarmDirection === SwarmMoveDirection.Down) {
                this.swarmDirection = SwarmMoveDirection.Left;
            } else {
                this.swarmDirection = SwarmMoveDirection.Down;
            }
        } else if (this.swarmPosition.x === 0) {
            if (this.swarmDirection === SwarmMoveDirection.Down) {
                this.swarmDirection = SwarmMoveDirection.Right;
            } else {
                this.swarmDirection = SwarmMoveDirection.Down;
            }
        }

        const speed: number = this.swarmPosition.y / AlienSwarm.SWARM_MOVE_GRID.y;
        await Promise.all(this.getAlienArray().map((x) => x.move(this.swarmDirection, 1 + speed)));
        
        switch (this.swarmDirection) {
            case SwarmMoveDirection.Right:
                this.swarmPosition.x++;
                break;
            case SwarmMoveDirection.Left:
                this.swarmPosition.x--;
                break;
            case SwarmMoveDirection.Up:
                this.swarmPosition.y--;
                break;
            case SwarmMoveDirection.Down:
                this.swarmPosition.y++;
                break;
        }

        if (this.active) {
            this.moveSwarm();
        }
    }

    private getAlienArray(): Alien[] {
        return this.aliens.reduce((array, aliens) => [...array, ...aliens.filter((x) => x !== undefined)], []) as Alien[];
    }

    private resetFireTimer(): void {
        this.fireTimer =
            (Math.random() * (AlienSwarm.MAX_FIRE_TIME_SECONDS - AlienSwarm.MIN_FIRE_TIME_SECONDS)) +
            AlienSwarm.MIN_FIRE_TIME_SECONDS;
    }

    private fireGun(): void {
        const exposedAliens: Alien[] = [];
        for (let x = 0; x < this.aliens.length; x++) {
            for (let y = this.aliens[x].length - 1; y >= 0; y--) {
                if (this.aliens[x][y]?.isAlive()) {
                    exposedAliens.push(this.aliens[x][y]!);
                    break;
                }
            }
        }

        const index: number = Math.floor(Math.random() * exposedAliens.length);
        const projectile: DisplayObject | undefined = exposedAliens[index].fireGun();
        if (projectile) {
            this.projectiles.push(projectile);
        }
    }
}