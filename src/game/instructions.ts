import { CollidableShape } from "libs/core-game/src/game/collision";
import { Renderable } from "libs/core-game/src/game/renderable";
import { FontStyling } from "libs/core-game/src/utils/font-styling";
import { LoadFont } from "libs/core-game/src/utils/load-font";
import { Application, Container, Graphics, Point, Sprite, Text } from "pixi.js";

export class Instructions extends Renderable {
    private container?: Container;

    constructor(id: string, app: Application, protected font: FontStyling) {
        super(id, app);
    }

    public init(): Promise<void> {
        return new Promise((resolve) => {
            this.app.loader.reset();
            this.app.loader.add("block", "./assets/block.png");
            this.app.loader.add("arrow-left", "./assets/arrow-left.png");
            this.app.loader.add("arrow-right", "./assets/arrow-right.png");
            this.app.loader.add("arrow-up", "./assets/arrow-up.png");
            this.app.loader.add("arrow-down", "./assets/arrow-down.png");
            this.app.loader.load(() => {
                LoadFont(this.font.family).then(() => {
                    this.container = new Container();

                    const graphic: Graphics = new Graphics();
                    graphic.beginFill(0x333333, 0.7);
                    graphic.drawRoundedRect(-20, -20, 410, 140, 20);
                    graphic.endFill();
                    this.container.addChild(graphic);

                    this.container.addChild(this.createMoveInstruction());
                    this.container.addChild(this.createFireInstruction());

                    this.container.x = 315;
                    this.container.y = 720;

                    this.app.stage.addChild(this.container);
                    resolve();
                });
            });
        });
    }

    public setVisible(visible: boolean): void {
        if (this.container) {
            this.container.visible = visible;
        }
    }

    public render(delta: number): void {
        // Do nothing.
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    private createMoveInstruction(): Container {
        const arrows: Container = this.createArrows(
            "block",
            "arrow-left",
            "arrow-right",
            "block"
        );
        const label: Text = new Text("Press the left and right arrows to\nmove the ship.");
        label.style.fontFamily = this.font.family;
        label.style.fontSize = 20;
        label.style.fill = this.font.color;
        label.anchor.set(0, 0);
        label.x = 80;
        label.y = 5;

        const container: Container = new Container();
        container.x = 0;
        container.y = 0;
        container.addChild(arrows, label);
        return container;
    }

    private createFireInstruction(): Container {
        const arrows: Container = this.createArrows(
            "arrow-up",
            "block",
            "block",
            "block"
        );
        const label: Text = new Text("Press the up arrow or space to fire\nthe gun.");
        label.style.fontFamily = this.font.family;
        label.style.fontSize = 20;
        label.style.fill = this.font.color;
        label.anchor.set(0, 0);
        label.x = 80;
        label.y = 5;

        const container: Container = new Container();
        container.x = 0;
        container.y = 60;
        container.addChild(arrows, label);
        return container;
    }

    private createArrows(up: string, left: string, right: string, down: string): Container {
        const container: Container = new Container();
        container.addChild(
            this.createArrow(up, new Point(20, 0)),
            this.createArrow(left, new Point(0, 20)),
            this.createArrow(right, new Point(40, 20)),
            this.createArrow(down, new Point(20, 20))
        )
        return container;
    }

    private createArrow(sprite: string, position: Point): Sprite {
        const arrow: Sprite = new Sprite(this.app.loader.resources[sprite].texture);
        arrow.x = position.x;
        arrow.y = position.y;
        arrow.width = 20;
        arrow.height = 20;
        return arrow;
    }
}