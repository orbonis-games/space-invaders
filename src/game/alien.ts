import { Easing, Tween } from "@tweenjs/tween.js";
import { CollidableShape } from "libs/core-game/src/game/collision";
import { Renderable } from "libs/core-game/src/game/renderable";
import { sfx } from "libs/core-game/src/game/sfx";
import { Emitter, EmitterConfig } from "pixi-particles";
import { Application, Container, DisplayObject, Graphics, Point, Texture } from "pixi.js";
import { AlienSwarm, SwarmMoveDirection } from "./alien-swarm";

export class Alien extends Renderable {
    protected readonly fireSpeed: number = 600;

    protected container?: Container;
    protected graphic?: DisplayObject;
    protected projectile?: Graphics;
    protected explosionContainer?: Container;
    protected explosion?: Emitter;
    protected alive: boolean = true;
    protected active: boolean = false;
    protected tweens: Tween<any>[] = [];

    constructor(id: string, app: Application, protected parent: Container, protected startPosition: Point, protected elite: boolean) {
        super(id, app);
    }

    public init(): Promise<void> {
        return new Promise((resolve) => {
            const setup = () => {
                this.container = new Container();
                this.parent.addChild(this.container);

                this.graphic = this.createAlien();
                this.container.addChild(this.graphic);

                const texture: Texture = this.app.loader.resources["particle"].texture;
                const config: EmitterConfig = this.app.loader.resources["explosion"].data;

                this.explosionContainer = new Container();
                this.container.addChild(this.explosionContainer);

                this.explosion = new Emitter(this.explosionContainer, texture, config);

                this.reset();
                resolve();
            };

            if ("explosion" in this.app.loader.resources && "particle" in this.app.loader.resources) {
                setup();
            } else {
                this.app.loader.reset();
                this.app.loader.add("particle", "./assets/particle.png");
                this.app.loader.add("explosion", "./assets/explosion.json");
                this.app.loader.load(setup);
            }
        });
    }

    public reset(): void {
        if (this.container && this.graphic && this.explosion) {
            this.container.x = 275 + (this.startPosition.x * 50);
            this.container.y = 250 + (this.startPosition.y * 35);
            this.graphic.visible = true;
            this.alive = true;

            for (const tween of this.tweens) {
                tween.stop();
            }

            this.tweens = [];
        }
    }

    public setActive(value: boolean): void {
        this.active = value;
    }

    public isAlive(): boolean {
        return this.alive;
    }

    public checkCollision(projectile?: DisplayObject): boolean {
        if (this.container && projectile && projectile.transform && this.alive && this.active) {
            const verticalCheck: boolean =
                (projectile.x >= this.container.x - (this.container.width / 2)) &&
                (projectile.x <= this.container.x + (this.container.width / 2));
            const horizontalCheck: boolean =
                (projectile.y >= this.container.y - (this.container.height / 2)) &&
                (projectile.y <= this.container.y + (this.container.height / 2));
            
            const collision: boolean = verticalCheck && horizontalCheck;
            if (collision) {
                this.destroy();
            }
            return collision;
        } else {
            return false;
        }
    }

    public render(delta: number): void {
        this.explosion?.update(delta);

        if (this.projectile) {
            if (this.projectile.transform === null) {
                this.projectile = undefined;
            } else {
                this.projectile.y += this.fireSpeed * delta;
    
                if (this.projectile.y > 1000) {
                    this.projectile.destroy();
                    this.projectile = undefined;
                }
            }
        }
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    public destroy(perminantly: boolean = false): void {
        if (perminantly && this.container) {
            this.container.destroy();
            this.container = undefined;
        } else if (this.container && this.explosion && this.graphic) {
            this.explosion.playOnce();
            this.graphic.visible = false;
            this.alive = false;

            sfx.play("explosion");
        }
    }

    public fireGun(): DisplayObject | undefined {
        if (!this.projectile && this.container && this.active) {
            this.projectile = new Graphics();

            this.projectile.lineStyle(2, 0xFFFFFF);
            this.projectile.drawPolygon([
                -5, 0,
                5, 0,
                0, 5
            ]);
            this.projectile.x = this.container.x;
            this.projectile.y = this.container.y + 20;

            this.app.stage.addChild(this.projectile);

            sfx.play("shoot");

            return this.projectile;
        } else {
            return undefined;
        }
    }

    public hasInvaded(): boolean {
        return this.alive && this.container !== undefined && this.container.y >= AlienSwarm.SWARM_INVASION_MARKER; 
    }

    public move(direction: SwarmMoveDirection, speed: number): Promise<void> {
        return new Promise<void>((resolve) => {
            if (this.container && this.alive && this.active) {
                const target: Point = new Point(this.container.x, this.container.y);

                switch (direction) {
                    case SwarmMoveDirection.Right:
                        target.x += this.container.width;
                        break;
                    case SwarmMoveDirection.Left:
                        target.x -= this.container.width;
                        break;
                    case SwarmMoveDirection.Down:
                        target.y += 26;
                        break;
                    case SwarmMoveDirection.Up:
                        target.y -= 26;
                        break;
                }

                const move = new Tween(this.container)
                    .to({ x: target.x, y: target.y }, 500 / speed)
                    .easing(Easing.Quadratic.InOut)
                    .onComplete(() => {
                        this.tweens.splice(this.tweens.indexOf(move), 1);
                    })
                    .start();
                const scale = new Tween(this.container.scale)
                    .to({ x: 0.9, y: 0.9 }, 250 / speed)
                    .yoyo(true)
                    .repeat(1)
                    .onComplete(() => {
                        this.tweens.splice(this.tweens.indexOf(scale), 1);
                        window.setTimeout(() => resolve(), 250 / speed);
                    })
                    .start();

                this.tweens.push(move, scale);
            } else {
                resolve();
            }
        });
    }

    public getScore(): number {
        return (this.elite) ? 200 : 100;
    }

    protected createAlien(): DisplayObject {
        const graphic: Graphics = new Graphics();
        graphic.lineStyle(2, 0xFFFFFF);
        graphic.moveTo(0, 0);
        graphic.beginFill(0x000000);
        graphic.drawEllipse(0, 0, 20, 12);
        graphic.endFill();

        graphic.drawEllipse(-7, -5, 2, 1);
        graphic.drawEllipse(7, -5, 2, 1);

        if (this.elite) {
            graphic.moveTo(-10, 5);
            graphic.arcTo(0, 0, 7, 0, 5);
            graphic.moveTo(10, 5);
            graphic.arcTo(0, 0, -7, 0, 5);
            graphic.moveTo(-10, 5);
            graphic.lineTo(10, 5);

            graphic.moveTo(-10, -12);
            graphic.lineTo(-10, -17);
            graphic.lineTo(-2, -12);
            graphic.moveTo(10, -12);
            graphic.lineTo(10, -17);
            graphic.lineTo(2, -12);
            graphic.moveTo(-5, -12);
            graphic.lineTo(-5, -17);
            graphic.lineTo(0, -12);
            graphic.moveTo(5, -12);
            graphic.lineTo(5, -17);
            graphic.lineTo(0, -12);
        } else {
            graphic.moveTo(-7, 5);
            graphic.arcTo(0, 0, -7, 0, 5);
            graphic.moveTo(7, 5);
            graphic.arcTo(0, 0, 7, 0, 5);
        }

        graphic.cacheAsBitmap = true;

        return graphic;
    }
}
