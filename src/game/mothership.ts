import { Easing, Tween } from "@tweenjs/tween.js";
import { CollidableShape } from "libs/core-game/src/game/collision";
import { Application, DisplayObject, Graphics, Point } from "pixi.js";
import { Alien } from "./alien";
import { SwarmMoveDirection } from "./alien-swarm";

export class Mothership extends Alien {
    public static readonly MIN_TIME_SECONDS: number = 10;
    public static readonly MAX_TIME_SECONDS: number = 30;

    private readonly speed: number = 300;

    private spawnTime: number = 0;

    constructor(id: string, app: Application) {
        super(id, app, app.stage, new Point(16, -1.5), true);
    }

    public reset(): void {
        super.reset();
        this.spawnTime = (Math.random() * (Mothership.MAX_TIME_SECONDS - Mothership.MIN_TIME_SECONDS)) + Mothership.MIN_TIME_SECONDS;
    }

    public render(delta: number): void {
        super.render(delta);

        if (this.container && this.alive && this.active) {
            this.spawnTime -= delta;

            if (this.spawnTime <= 0) {
                this.container.x -= delta * this.speed;

                if (this.container.x < -this.container.width) {
                    this.reset();
                }
            }
        }
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    public destroy(): void {
        if (this.container && this.explosion && this.graphic) {
            this.explosion.playOnce(() => this.reset());
            this.graphic.visible = false;
            this.alive = false;
        }
    }

    public move(direction: SwarmMoveDirection, speed: number): Promise<void> {
        return new Promise<void>((resolve) => {
            if (this.container && this.alive && this.active) {
                const target: Point = new Point(this.container.x, this.container.y);

                switch (direction) {
                    case SwarmMoveDirection.Right:
                        target.x += this.container.width;
                        break;
                    case SwarmMoveDirection.Left:
                        target.x -= this.container.width;
                        break;
                    case SwarmMoveDirection.Down:
                        target.y += 26;
                        break;
                    case SwarmMoveDirection.Up:
                        target.y -= 26;
                        break;
                }

                const move = new Tween(this.container)
                    .to({ x: target.x, y: target.y }, 500 / speed)
                    .easing(Easing.Quadratic.InOut)
                    .onComplete(() => {
                        this.tweens.splice(this.tweens.indexOf(move), 1);
                    })
                    .start();

                this.tweens.push(move);
            } else {
                resolve();
            }
        });
    }

    public getScore(): number {
        return 1000;
    }

    protected createAlien(): DisplayObject {
        const graphic: Graphics = new Graphics();
        graphic.lineStyle(2, 0xFFFFFF);
        graphic.moveTo(0, 0);
        graphic.beginFill(0x00000);
        graphic.drawEllipse(0, 0, 40, 12);
        graphic.endFill();
        graphic.lineStyle(2, 0xFFFFFF, 0.8);
        graphic.drawEllipse(0, -7, 15, 2);
        graphic.lineStyle(2, 0xFFFFFF);
        graphic.drawEllipse(-33, 0, 2, 1);
        graphic.drawEllipse(-20, 3, 2, 1);
        graphic.drawEllipse(-7, 5, 2, 1);
        graphic.drawEllipse(7, 5, 2, 1);
        graphic.drawEllipse(20, 3, 2, 1);
        graphic.drawEllipse(33, 0, 2, 1);

        graphic.cacheAsBitmap = true;

        return graphic;
    }
}